package com.coolJavaProjects;

public abstract class Candidate implements InterfaceName  {                     //interface helps select correct method depending on object class (Polymorphism)

    String candidateName;

    public Candidate(String candidateName){
        this.candidateName = candidateName;
    }
    public abstract void hello();
    // public abstract void describeExperience();
}
class Graduated extends Candidate {                     //Graduated class inherits candidateName variable from superclass Candidate
    public Graduated(String candidateName) {
        super(candidateName);
    }
    public void describeExperience(){
        System.out.println("I passed successfully getJavaJob exams and code reviews.");
    }
    public void hello() {
        System.out.println("hi! my name is " + candidateName + "!");
    }

}
 class SelfLearners extends Candidate {               //SelfLearners class inherits candidateName variable from superclass Candidate
     public SelfLearners(String candidateName) {
         super(candidateName);
     }
     public void describeExperience() {
                System.out.println("I have been learning Java by myself, nobody examined how thorough is my knowledge and how good is my code.");
     }
     public void hello() {
         System.out.println("hi! my name is " + candidateName + "!");
     }
 }