package com.coolJavaProjects;

public class Main {

    public static void main(String[] args) {

        SelfLearners n1 = new SelfLearners("Divock Origi");
        Graduated n2 = new Graduated("Jonathan Joestar");
        SelfLearners n3 = new SelfLearners("Georginio Wijnaldum");
        Graduated n4 = new Graduated("Joseph Joestar");
        Graduated n5 = new Graduated("Jotaro Kujo");
        Graduated n6 = new Graduated("Josuke Higashikata");
        Graduated n7 = new Graduated("Giorno Giovanna");
        SelfLearners n8 = new SelfLearners("Sadio Mané");
        SelfLearners n9 = new SelfLearners("Joël Matip");
        SelfLearners n10 = new SelfLearners("Joske Joestar");

        Candidate[] candidates = new Candidate[10];
        candidates[0] = n1;
        candidates[1] = n2;
        candidates[2] = n3;
        candidates[3] = n4;
        candidates[4] = n5;
        candidates[5] = n6;
        candidates[6] = n7;
        candidates[7] = n8;
        candidates[8] = n9;
        candidates[9] = n10;

        Employer employer = new Employer();

        for (Candidate candidate : candidates) {
            employer.hello();                               // Employer class Encapsulation
            candidate.hello();                              // Candidate class Encapsulation
            candidate.describeExperience();                 // Candidate class Encapsulation
        }
    }
}



